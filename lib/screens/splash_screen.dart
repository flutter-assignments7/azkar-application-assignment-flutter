import 'dart:ui';
import 'package:azkar_app/widgets/azcar_text.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';

class SplashScreen extends StatefulWidget {
  @override
  _SplashScreenState createState() => _SplashScreenState();
}

class _SplashScreenState extends State<SplashScreen> {
  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    Future.delayed(Duration(seconds: 3),(){
      Navigator.pushReplacementNamed(context, '/azcar_screen');
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: AnnotatedRegion<SystemUiOverlayStyle>(
        value: SystemUiOverlayStyle.dark.copyWith(
          statusBarColor: Theme.of(context).disabledColor,
        ),
        child: Stack(
          children: [
            Image.asset(
              'images/sobha.png',
              height: double.infinity,
              width: double.infinity,
              fit: BoxFit.cover,
            ),
            Column(
              mainAxisAlignment: MainAxisAlignment.center,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                Container(
                  width: 170,
                  alignment: Alignment.center,
                  padding: EdgeInsets.symmetric(horizontal: 5),
                  decoration: BoxDecoration(
                    borderRadius: BorderRadius.only(
                      topLeft: Radius.circular(20),
                      bottomRight: Radius.circular(20),
                    ),
                    border: Border.all(
                      width: 3,
                      color: Colors.white,
                    ),
                  ),
                  child: AzcarText(
                    text: 'سُبحة الأذكار',
                    fontWeight: FontWeight.bold,
                    fontSize: 25,
                    color: Colors.white,
                    textAlign: TextAlign.center,
                  ),
                ),
                Container(
                  alignment: Alignment.center,
                  margin: EdgeInsets.only(top: 5),
                  child: AzcarText(
                    text: 'ألا بذكر الله تطمئن القلوب',
                    fontSize: 12,
                    color: Colors.white,
                  ),
                ),
              ],
            ),
            Positioned(
              bottom: 10,
              left: 0,
              right: 0,
              child: AzcarText(
                text: 'PalLancer - الكلية الجامعية',
                textAlign: TextAlign.center,
                color: Colors.white,
              ),
            ),
          ],
        ),
      ),
    );
  }
}
