import 'package:azkar_app/widgets/azcar_text.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class AzcarScreen extends StatefulWidget {
  @override
  _AzcarScreenState createState() => _AzcarScreenState();
}

class _AzcarScreenState extends State<AzcarScreen> {
  int _counter = 0;
  String _zaker = 'استغفر الله';

  @override
  Widget build(BuildContext context) {
    return Directionality(
      textDirection: TextDirection.rtl,
      child: Scaffold(
        appBar: AppBar(
          backgroundColor: Color(0xFF00796b),
          centerTitle: true,
          title: AzcarText(
            text: 'سُبحة الأذكار',
            fontSize: 18,
            color: Colors.white,
            fontWeight: FontWeight.bold,
          ),
          actions: [
            PopupMenuButton<String>(
              offset: Offset(20, 43),
              onSelected: (String selectedItem) {
                switch (selectedItem) {
                  case 'z1':
                    changeItem('سبحان الله');
                    break;
                  case 'z2':
                    changeItem('استغفر الله');
                    break;
                }
              },
              itemBuilder: (context) {
                return [
                  PopupMenuItem(
                    child: AzcarText(
                      text: 'سبحان الله',
                      fontSize: 14,
                    ),
                    value: 'z1',
                  ),
                  PopupMenuItem(
                    child: AzcarText(
                      text: 'استغفر الله',
                      fontSize: 14,
                    ),
                    value: 'z2',
                  ),
                ];
              },
            ),
          ],
        ),
        body: Stack(
          children: [
            Image.asset(
              'images/sobha.png',
              height: double.infinity,
              width: double.infinity,
              fit: BoxFit.cover,
            ),
            Padding(
              padding: const EdgeInsets.symmetric(horizontal: 20),
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Card(
                    margin: EdgeInsetsDirectional.only(top: 20),
                    elevation: 0,
                    color: Colors.white70,
                    shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(20),
                    ),
                    child: Padding(
                      padding: const EdgeInsets.all(10),
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.end,
                        children: [
                          CircleAvatar(
                            backgroundImage: AssetImage('images/image_1.jpg'),
                            radius: 22,
                          ),
                          SizedBox(
                            width: 10,
                          ),
                          AzcarText(
                            text: 'Ibrahim Mohammed',
                            fontWeight: FontWeight.bold,
                          ),
                          Spacer(),
                          IconButton(
                            onPressed: () {
                              Navigator.pushNamed(
                                context,
                                '/about_screen',
                                arguments: {'title':'About Me'},
                              );
                            },
                            icon: Icon(
                              Icons.info,
                              color: Color(0xFF00796b),
                            ),
                            padding: EdgeInsets.zero,
                            alignment: Alignment.centerLeft,
                          ),
                        ],
                      ),
                    ),
                  ),
                  Spacer(),
                  Card(
                    clipBehavior: Clip.antiAlias,
                    shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(30),
                      side: BorderSide(
                        width: 2,
                        color: Color(0xFF00796b),
                      ),
                    ),
                    margin: EdgeInsets.only(bottom: 12),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        Expanded(
                          child: AzcarText(
                            text: _zaker,
                            fontWeight: FontWeight.bold,
                            textAlign: TextAlign.center,
                          ),
                        ),
                        Container(
                          width: 45,
                          height: 45,
                          decoration: BoxDecoration(
                            color: Color(0xFF009688),
                            // borderRadius: BorderRadius.only(
                            //   topLeft: Radius.circular(30),
                            //   bottomLeft: Radius.circular(30),
                            // ),
                            boxShadow: [
                              BoxShadow(
                                // color:
                                color: Colors.grey.withOpacity(1),
                                offset: Offset(3, 0),
                                blurRadius: 5,
                                spreadRadius: 2,
                              ),
                            ],
                          ),
                          child: AzcarText(
                            text: _counter.toString(),
                            color: Colors.white,
                            fontSize: 22,
                            textAlign: TextAlign.center,
                          ),
                        ),
                      ],
                    ),
                  ),
                  Row(
                    children: [
                      Expanded(
                        flex: 2,
                        child: ElevatedButton(
                          onPressed: () {
                            setState(() {
                              ++_counter;
                            });
                          },
                          child: AzcarText(
                            text: 'تسبيح',
                            color: Colors.white,
                            fontWeight: FontWeight.bold,
                          ),
                          style: ElevatedButton.styleFrom(
                            primary: Color(0xFF009688),
                            shape: RoundedRectangleBorder(
                              borderRadius: BorderRadius.only(
                                  topRight: Radius.circular(20),
                                  bottomRight: Radius.circular(40)),
                            ),
                          ),
                        ),
                      ),
                      Expanded(
                        flex: 1,
                        child: ElevatedButton(
                          onPressed: () {
                            setState(() {
                              _counter = 0;
                            });
                          },
                          child: AzcarText(
                            text: 'مسح',
                            color: Colors.white,
                          ),
                          style: ElevatedButton.styleFrom(
                            primary: Color(0xFFe57373),
                            shape: RoundedRectangleBorder(
                              borderRadius: BorderRadius.only(
                                  topLeft: Radius.circular(40),
                                  bottomLeft: Radius.circular(20)),
                            ),
                          ),
                        ),
                      ),
                    ],
                  ),
                  Spacer(),
                ],
              ),
            ),
            Positioned(
              bottom: 10,
              left: 0,
              right: 0,
              child: AzcarText(
                text: 'الكلية الجامعية - PalLancer',
                color: Colors.white,
                textAlign: TextAlign.center,
              ),
            ),
          ],
        ),
      ),
    );
  }

  void changeItem(String item) {
    if (_zaker != item) {
      setState(() {
        _zaker = item;
        _counter = 0;
      });
    }
  }
}
