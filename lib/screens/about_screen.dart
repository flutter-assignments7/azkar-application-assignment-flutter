import 'package:azkar_app/widgets/azcar_text.dart';
import 'package:azkar_app/widgets/azkar_container_tile.dart';
import 'package:flutter/material.dart';

class AboutScreen extends StatelessWidget {
  late Map<String, dynamic> data = {};

  @override
  Widget build(BuildContext context) {
    ModalRoute? modalRoute = ModalRoute.of(context);
    if (modalRoute != null)
      data = modalRoute.settings.arguments as Map<String, dynamic>;

    return Scaffold(
      appBar: AppBar(
        backgroundColor: Color(0xFF00796b),
        centerTitle: true,
        title: AzcarText(
          text: data['title'],
          fontSize: 18,
          color: Colors.white,
          fontWeight: FontWeight.bold,
        ),
      ),
      body: Stack(
        children: [
          Image.asset(
            'images/sobha.png',
            height: double.infinity,
            width: double.infinity,
            fit: BoxFit.cover,
          ),
          Column(
            children: [
              SizedBox(
                height: 10,
              ),
              CircleAvatar(
                radius: 50,
                backgroundImage: AssetImage('images/ibra.jpg'),
              ),
              SizedBox(
                height: 5,
              ),
              AzcarText(
                text: 'Ibrahim Mohammed',
                fontWeight: FontWeight.bold,
                fontSize: 20,
                decoration: TextDecoration.underline,
              ),
              AzcarText(
                text: 'Flutter Developer',
                color: Colors.white70,
                fontWeight: FontWeight.bold,
              ),
              Divider(
                color: Colors.grey.shade700,
                thickness: 0.5,
                height: 40,
              ),
              AzkarContainerTile(
                iconDetails: Icons.email,
                iconColor: Color(0xFF00796b),
                title: 'Email',
                supTitle: 'Dev.IbrahimMH@gmail.com',
                iconButton: Icons.arrow_forward_ios,
                iconButtonColor: Colors.white70,
              ),
              Divider(
                color: Colors.grey.shade700,
                thickness: 0.5,
                height: 20,
              ),
              AzkarContainerTile(
                iconDetails: Icons.phone_android,
                iconColor: Color(0xFF00796b),
                title: 'Phone Numper',
                supTitle: '+970 597 817 052',
                iconButton: Icons.phone,
                iconButtonColor: Colors.white70,
              ),
              Divider(
                color: Colors.grey.shade700,
                thickness: 0.5,
                height: 20,
              ),
              AzkarContainerTile(
                iconDetails: Icons.add_location,
                iconColor: Color(0xFF00796b),
                title: 'Address',
                supTitle: 'Palestine - Gaza - Rafah',
                iconButton: Icons.add_location_outlined,
                iconButtonColor: Colors.white70,
              ),
            ],
          ),
          Positioned(
            bottom: 10,
            left: 0,
            right: 0,
            child: AzcarText(
              text: 'PalLancer - الكلية الجامعية',
              textAlign: TextAlign.center,
              color: Colors.white,
            ),
          ),
        ],
      ),
    );
  }
}
