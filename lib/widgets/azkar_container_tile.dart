import 'package:azkar_app/widgets/azcar_text.dart';
import 'package:flutter/material.dart';

class AzkarContainerTile extends StatelessWidget {
  final double marginStart;
  final double marginEnd;
  final IconData iconDetails;
  final Color iconColor;
  final double iconSize;
  final String title;
  final String supTitle;
  final IconData iconButton;
  final Color iconButtonColor;
  final VoidCallback? onPressed;

  AzkarContainerTile({
    this.marginStart = 10,
    this.marginEnd = 30,
    required this.iconDetails,
    this.iconColor = Colors.transparent,
    this.iconSize = 30,
    required this.title,
    required this.supTitle,
    required this.iconButton,
    this.iconButtonColor = Colors.transparent,
    this.onPressed,
  });

  @override
  Widget build(BuildContext context) {
    return Container(
      child: Row(
        children: [
          Container(
            margin: EdgeInsetsDirectional.only(start: marginStart, end: marginEnd),
            child: Icon(
              iconDetails,
              color: iconColor,
              size: iconSize,
            ),
          ),
          //   padding: EdgeInsetsDirectional.only(
          //     start: 8,
          //     top: 8,
          //     bottom: 8,
          //     end: 30,
          //   ),
          // ),
          Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              AzcarText(
                text: title,
                fontWeight: FontWeight.bold,
              ),
              AzcarText(
                text: supTitle,
                color: Colors.white70,
              ),
            ],
          ),
          Spacer(),
          IconButton(
            onPressed: onPressed,
            icon: Icon(
              iconButton,
              color:  iconButtonColor,
            ),
          ),
        ],
      ),
    );
  }
}
