import 'package:azkar_app/screens/about_screen.dart';
import 'package:azkar_app/screens/azcar_screen.dart';
import 'package:azkar_app/screens/splash_screen.dart';
import 'package:flutter/material.dart';

void main() => runApp(MainApp());


class MainApp extends StatelessWidget {

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      initialRoute: '/splash_screen',

      routes: {

        '/splash_screen' : (context) => SplashScreen(),
        '/azcar_screen' : (context) => AzcarScreen(),
        '/about_screen' : (context) => AboutScreen(),

      },

    );
  }
}
